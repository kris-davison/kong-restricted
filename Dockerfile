FROM kong:0.14.0-alpine

ADD https://github.com/krallin/tini/releases/download/v0.18.0/tini-static /sbin/tini

ENTRYPOINT ["/sbin/tini", "--", "/docker-entrypoint.sh"]

CMD ["kong", "docker-start"]

RUN addgroup -g 1000 -S appuser && \
    adduser -u 1000 -S appuser -G appuser && \
    chown appuser:appuser /usr/local/kong && \
    chown appuser:appuser /sbin/tini && \
    chmod 700 /sbin/tini
USER appuser
